import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TesteIMC {
	public static void main(String[] args) {
		File arquivoCSV = new File("C:\\DATASET.csv");
		try (Scanner leitor = new Scanner(arquivoCSV)) {
			// Leitura do arquivo
			leitor.nextLine();
			FileWriter salvar = new FileWriter("meuNomeCompleto.txt");
			while (leitor.hasNextLine()) {
				String linha = leitor.nextLine();
				String[] valores = linha.split(";");
				if (valores.length > 2) {
					double peso = Double.parseDouble(valores[2].replace(",", "."));
					double altura = Double.parseDouble(valores[3].replace(",", "."));
					double imc = peso / (altura * altura);
					if(imc < 1) {
						System.out.println("Verifique os dados para o calculo IMC de " +valores[0].toUpperCase() +" "+ valores[1].toUpperCase());
					} else {
						String result = valores[0].toUpperCase() + " " + valores[1].toUpperCase() + " "
								+ String.format("%.2f", imc);
						System.out.println(result.replaceAll("\\s+"," ").trim());
						salvar.write(result + System.lineSeparator());
					}

				} else { 
					System.out.println("Verifique os dados para o calculo IMC de " +valores[0].toUpperCase() + valores[1].toUpperCase());
				}
			}
			salvar.close();

		} catch (FileNotFoundException e) {
			System.err.println("Arquivo não encontrado: ");
		} catch (NumberFormatException e) {
			System.err.println("O valor não é um número válido");
		} catch (IOException e) {
			System.err.println("Erro ao salvar dados: ");
		}
	}
}
